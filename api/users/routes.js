const express = require("express");
const router = express.Router();
const User = require("./model");

router.get("/", (req, res) => {
  User.find()
    .exec()
    .then(result => res.send(result))
    .catch(e => res.status(500).send(e));
});

router.get("/:id", async (req, res) => {
  User.findById(req.params.id)
    .exec()
    .then(result => res.send(result))
    .catch(e => res.status(500).send(e));
});

router.post("/", async (req, res) => {
  const { name, age, country } = req.body;
  User.create({ name, age, country })
    .then(result => res.send(result))
    .catch(e => res.status(500).send(e));
});

router.put("/:id", async (req, res) => {
  const { name, age, country } = req.body;
  User.update({ _id: req.params.id }, { name, age, country })
    .then(result => res.send(result))
    .catch(e => res.status(500).send(e));
});

router.delete("/:id", async (req, res) => {
  User.deleteOne({ _id: req.params.id })
    .then(result => res.send(result))
    .catch(e => res.status(500).send(e));
});
module.exports = router;
