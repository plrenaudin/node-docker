const express = require("express");
const cors = require("cors");
const app = express();
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
mongoose.connect(`mongodb://db:${process.env.DB_PORT}/node-docker`, {
  useNewUrlParser: true
});

const usersRoute = require("./users/routes.js");

app.use(cors());

app.use(bodyParser.json());

app.use("/users", usersRoute);

app.use((err, req, res) => {
  console.error(err.stack);
  res.status(500).send("Something broke!");
});

app.listen(process.env.API_PORT, () => console.log(`Example app listening on ports ${process.env.API_PORT}!`));
